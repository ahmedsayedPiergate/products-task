﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ProductManagement.Models
{
    public class Product
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string ProductImage { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        [Ignore]
        public List<string> Gallery { get; set; }
        [Ignore]
        public int? Rate { get; set; }
    }
}
