﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProductManagement.Helpers.ExtensionMethods;
using ProductManagement.Models;
using ProductManagement.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProductManagement.ViewModels
{
	public class ProductListPageViewModel : BindableBase
	{
        #region Properties
        public ICommand ItemTappedCommand { get; set; }
        public ObservableCollection<Product> Products
        {
            get
            {
                return _Products;
            }
            set
            {
                _Products = value;
                RaisePropertyChanged();
            }
        }
        public Product SelectedProduct { get; set; }
        IProductService productService;
        private INavigationService _navigationService;
        private ObservableCollection<Product> _Products = new ObservableCollection<Product>();
        private string _searchText;
        private ObservableCollection<Product> preservedProducts = new ObservableCollection<Product>();

        public string searchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value;
                SearchBar();
                RaisePropertyChanged();
            }
        }
        #endregion

        public ProductListPageViewModel(INavigationService navigationService, IProductService productService)
        {
            this.productService = productService;
            _navigationService = navigationService;
            ItemTappedCommand = new Command(ItemTappedCommandActionAsync);
            Products = productService.GetProducts().ToObservableCollection();
            preservedProducts = Products;
        }

        private async void ItemTappedCommandActionAsync(object obj)
        {
            var product = obj as Product;
            var Parameters = new NavigationParameters();
            Parameters.Add("Product", product);
            await _navigationService.NavigateAsync("ProductPage",Parameters);
        }
        private void SearchBar()
        {
            if (searchText.Length > 2 && Products.Any(a => a.ProductName.ToLower().Contains(searchText.ToLower())))
            {
                Products = Products.Where(a => a.ProductName.ToLower().Contains(searchText.ToLower())).Select(a => a).ToObservableCollection();
            }
            else
            {
                Products = preservedProducts;
            }
        }
    }
}
