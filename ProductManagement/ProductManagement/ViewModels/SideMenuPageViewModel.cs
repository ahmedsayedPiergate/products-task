﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProductManagement.ViewModels
{
	public class SideMenuPageViewModel : BindableBase
	{
        public DelegateCommand<string> NavigateCommand { get; set; }
        INavigationService _navigationService;
        public SideMenuPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateCommand = new DelegateCommand<string>(NavigateActionAsync);
        }

        private async void NavigateActionAsync(string page)
        {
            await _navigationService.NavigateAsync(new Uri(page,UriKind.Relative));
        }
    }
}
