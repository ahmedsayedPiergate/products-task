﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProductManagement.Models;
using ProductManagement.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProductManagement.ViewModels
{
	public class ProductPageViewModel : BindableBase,INavigatedAware
	{
        public ICommand AddFavouriteCommand { get; set; }
        public Product product {
            get {
                return _product;
            }
            set
            {
                _product = value;
                RaisePropertyChanged();
            }
        }
        private List<string> _GalleryForAll=new List<string>();
        private Product _product;

        public ProductPageViewModel()
        {
            AddFavouriteCommand = new Command(AddFavouriteAsync);            
        }

        private async void AddFavouriteAsync(object obj)
        {
            var favourites = await DbSingleton.Database.GetProductsAsync();
            if(!favourites.Contains(product))
            await DbSingleton.Database.SaveProductAsync(product);
            
           
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
         
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Product"))
            {
                product = (Product)parameters["Product"];
            }
           
            if (parameters.ContainsKey("FavouriteItem"))
            {
                product = (Product)parameters["FavouriteItem"];
                product.Gallery =new List<string> {
              "https://images-na.ssl-images-amazon.com/images/I/71p%2Bnb8nEeL._SX569_.jpg",
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjLdUT7gsC8FbtkznvuekhmtFztA3dPtjUyUOKRURInK3bs7Dvew",
            };
            }
        }
    }
}
