﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProductManagement.Models;
using ProductManagement.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProductManagement.ViewModels
{
	public class FavouritePageViewModel : BindableBase
	{
        public ICommand ItemTappedCommand { get; set; }
        public Product SelectedItem { get; set; }
        private List<Product> _FavouriteProducts=new List<Product>();

        public List<Product> FavouriteProducts { get
            {
                return _FavouriteProducts;
            }
            set
            {
                _FavouriteProducts = value;
                RaisePropertyChanged();
            }
        }

        INavigationService navigationService;
        public FavouritePageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            ItemTappedCommand = new Command(ItemTappedAsync);
            Task.Run(async () => FavouriteProducts = await DbSingleton.Database.GetProductsAsync());
        }

        private async void ItemTappedAsync(object obj)
        {
            var Parameters = new NavigationParameters();
            Parameters.Add("FavouriteItem",SelectedItem);
            await navigationService.NavigateAsync("ProductPage", Parameters);
        }
    }
}
