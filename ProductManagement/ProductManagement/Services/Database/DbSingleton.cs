﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProductManagement.Services.Database
{
    public class DbSingleton
    {
        static Database database;

        public static Database Database
        {
            get
            {
                if (database == null)
                {
                    database = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Products.db3"));
                }
                return database;
            }
        }
    }
}
