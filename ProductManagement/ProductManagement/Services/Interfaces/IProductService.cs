﻿using ProductManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManagement.Services.Interfaces
{
    public interface IProductService
    {
        List<Product> GetProducts();
    }
}
