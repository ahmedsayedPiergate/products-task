﻿using ProductManagement.Models;
using ProductManagement.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ProductManagement.Services
{
    public class ProductServices : IProductService
    {
        public List<Product> GetProducts()
        {

            var GalleyForAll = new List<string> {
              "https://images-na.ssl-images-amazon.com/images/I/71p%2Bnb8nEeL._SX569_.jpg",
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjLdUT7gsC8FbtkznvuekhmtFztA3dPtjUyUOKRURInK3bs7Dvew",
            };
            var products = new List<Product> {
                new Product{Id=0, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRevPKuwNHgsesrqV7LU0LBCwHR8e4qxhG4dVlCFuqR-3nrxLmC_A",ProductName="nokia 7.1",ProductPrice=4000,Gallery=GalleyForAll },
                new Product{Id=1, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJ9HKqKf--VDT2AYjskKKmUCJCrUk93fTyY3wyanzaQDY5x9QHjA",ProductName="pixel 3",ProductPrice=4999,Gallery=GalleyForAll},
                new Product{Id=2, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwxueWyt2Cexfl-sR-WHHN_ZDQrDBUO1NKc9sKtqPpnkriuKWl",ProductName="xiaomi redmi note 7",ProductPrice=3999,Gallery=GalleyForAll},
                new Product{Id=3, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhMY5TLdr2M_C1EZlamER6URKlin7GV3Bj-Qz9EpQUcp3Svel7",ProductName="Honor",ProductPrice=3999,Gallery=GalleyForAll},
                new Product{Id=4, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8nPXoZcZDMspHo9vxUf2f9lXVbNv1Y7UlFTMKieA_WURkoIiD" ,ProductName="Iphone 6",ProductPrice=5000,Gallery=GalleyForAll},
                new Product{Id=5, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuckdNxJX3GgttvrGmSrvpUIkSSS9TFQXMyVlUxeuSdK5V7ykR",ProductName="Huawei mate 20",ProductPrice=5999 ,Gallery=GalleyForAll},
                new Product{Id=6, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVff-mR0e4p_5A3G21HBSi8Et1zUO95-aRJXo8_KALYloYh5q_vw",ProductName="Huawei mate10",ProductPrice=3999 ,Gallery=GalleyForAll},
                new Product{Id=7, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgefcxv02hxEFaJ4Qt0RiIET9z4K8YcR0gB6-w76Nbs3elu9xG",ProductName="Lenovo",ProductPrice=555 ,Gallery=GalleyForAll},
                new Product{Id=8, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAR8CQQgRg5b2ghFGwhqCcHFAwSWF_CFWE7KzYCK9YCwWvy633sg",ProductName="Alcatel" ,ProductPrice=499 ,Gallery=GalleyForAll},
                new Product{Id=9, ProductImage="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDqhBJDQ6qA7El05SyCtyoENQPGHlG-xq77cni5Wweyh88168P",ProductName="Samsung S9",ProductPrice=9999M ,Gallery=GalleyForAll},
            };

            return products;
        }
    }
}
